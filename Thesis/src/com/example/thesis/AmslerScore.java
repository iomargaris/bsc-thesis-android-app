package com.example.thesis;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class AmslerScore extends Activity {
		
	TextView textV ;
	String result;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.amsler_score_layout);
        
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("PassFail");
        
        textV = (TextView)findViewById(R.id.amslerScore);

        
        
        
        if(value.equals("Passed") ){
        	//result = "Congratulations, there are no signs to suggest macular damage";
        	//Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        	textV.setText(R.string.amslerPassed);
        }
        else if(value.equals("Failed") )
        {
        	//Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        	//result = "Sorry, the test revealed that you may suffer from macular damage. It is highly advisable to contact your ophthalmologist. ";
        	textV.setText(R.string.amslerFailed);
        }
        
        
        
    	}
}
