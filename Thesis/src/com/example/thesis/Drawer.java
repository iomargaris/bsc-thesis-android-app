package com.example.thesis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Drawer extends Activity implements OnItemClickListener {
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle drawerListener;
	private MyAdapterDrawer myAdapterDrawer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawer_layout);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.drawerList);
		
		// Add header to the list
		View header = getLayoutInflater().inflate(R.layout.header, null);
		mDrawerList.addHeaderView(header);
				
		Toast.makeText(getApplicationContext(), 
                "Warning!!! Oculus is for screening only.\nIt is not a diagnosis.", Toast.LENGTH_SHORT).show();
		
		// Set the adapter for the list view
		myAdapterDrawer = new MyAdapterDrawer(this);
		mDrawerList.setAdapter(myAdapterDrawer);
		
		// Set the lists click listener
		mDrawerList.setOnItemClickListener(this);
		
		
		drawerListener = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_closed) {
			@Override
			public void onDrawerClosed(View drawerView) {
				// TODO Auto-generated method stub
				//Toast.makeText(Drawer.this, "Drawer Closed", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				// TODO Auto-generated method stub
				//Toast.makeText(Drawer.this, "Drawer Opended", Toast.LENGTH_SHORT).show();
			}
		};
			mDrawerLayout.setDrawerListener(drawerListener);
			getActionBar().setHomeButtonEnabled(true);
			getActionBar().setDisplayHomeAsUpEnabled(true);
		
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		drawerListener.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(drawerListener.onOptionsItemSelected(item))
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		drawerListener.syncState();
	}
	
	// Manage drawer list on item clicks
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		selectItem(position);
		switch (position) {
		case 1:
			Intent showTests = new Intent(Drawer.this, ListActivityWithImages.class);
            startActivity(showTests);
			break;
		case 2:
			/*Intent i = new Intent(Drawer.this, ListActivityWithImages.class);
            startActivity(i);*/
			break;
		case 3:
			Intent showNutrition = new Intent(Drawer.this, Nutrition.class);
            startActivity(showNutrition);
			break;
		case 4:
			Intent showAbout = new Intent(Drawer.this, About.class);
            startActivity(showAbout);
			break;
		}
	}

	// When an item is pressed, setTitle is called to set the action bar title
	public void selectItem(int position) {
		mDrawerList.setItemChecked(position, true);
		
	}

	// setTitle is implemented here
	public void setTitle(String title) {
		getActionBar().setTitle(title);
	}

}

class MyAdapterDrawer extends BaseAdapter {
	//Responsible for fetching my data from string
	private Context context;
	String[] loginMenu;
	int[] images_drawer = {R.drawable.tests, R.drawable.options,R.drawable.nutrition, R.drawable.about};
	
	public MyAdapterDrawer(Context context){
		this.context = context;
		loginMenu = context.getResources().getStringArray(R.array.Login);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return loginMenu.length ;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return loginMenu[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = null;
		if (convertView == null) {
			
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.single_row_drawer, parent, false);
		} 
		else 
		{
			row = convertView;
		}
		
		TextView titleTextView = (TextView) row.findViewById(R.id.textViewDrawer);
		ImageView titleImageView = (ImageView) row.findViewById(R.id.imageViewDrawer);
		
		titleTextView.setText(loginMenu[position]);
		titleImageView.setImageResource(images_drawer[position]);
		return row;
	}
	
	
	
}








