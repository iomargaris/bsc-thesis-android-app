package com.example.thesis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Amsler extends Activity {
	ImageView display ;
	Button mYesButton;
	Button mNoButton;
	String scoreAmsler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.amsler_layout);
        
        display = (ImageView)findViewById(R.id.display_amsler);
        display.setImageResource(R.drawable.amsler);
        
        
        mYesButton = (Button)findViewById(R.id.yes_button);
        mYesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	scoreAmsler = "Passed";
            	Toast.makeText(Amsler.this, "You " + scoreAmsler + " the Test", Toast.LENGTH_SHORT).show();
            	
            	Intent scoreAmsler = new Intent(getApplicationContext(), AmslerScore.class);
            	scoreAmsler.putExtra("PassFail","Passed");
            	startActivity(scoreAmsler);
            }
        });	
		
        mNoButton = (Button)findViewById(R.id.no_button);
        mNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	scoreAmsler = "Failed";
            	Toast.makeText(Amsler.this, "You " + scoreAmsler + " the Test", Toast.LENGTH_SHORT).show();
            	
            	Intent scoreAmsler = new Intent(getApplicationContext(), AmslerScore.class);
            	scoreAmsler.putExtra("PassFail","Failed");
            	startActivity(scoreAmsler);
            }
        });	
        
    	}
}
