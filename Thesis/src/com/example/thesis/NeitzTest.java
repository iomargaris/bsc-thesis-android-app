package com.example.thesis;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;


public class NeitzTest extends Activity  {
	
	ImageView display ;
	ImageButton imgButton,imgButton2,imgButton3,imgButton4,imgButton5;
	//Button mNextButton;
	int mCurrentIndex = 0;
	
	String realAnswers[] = {"square","circle","circle","square","triangle","triangle","circle","square","triangle"};
	List<String> realAnswersList = new ArrayList<String>();
	String usrAnswers[];
	List<String> usrAnswersList = new ArrayList<String>();
	
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.neitz_layout);
        
        //Populate the list with the correct answers
        for (int index = 0; index < realAnswers.length; index++)
        {
            realAnswersList.add(realAnswers[index]);
        }
        
        display = (ImageView)findViewById(R.id.display_neitz);
        updateImage();
        
        // This imageButton is for the Circle
        imgButton = (ImageButton)findViewById(R.id.imageButton);
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentIndex++;
                updateImage();
                
                usrAnswersList.add("circle");
                
                if (mCurrentIndex == 9 ){
                	scoreNeitz();
                }
            }
        });	
        
        // This imageButton is for the triangle
        imgButton2 = (ImageButton)findViewById(R.id.imageButton2);
        imgButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	mCurrentIndex++;
                updateImage();
                
                usrAnswersList.add("triangle");
                
                if (mCurrentIndex == 9 ){
                	scoreNeitz();
                }
            }
        });	
        
        // This imageButton is for the square
        imgButton3 = (ImageButton)findViewById(R.id.imageButton3);
        imgButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	mCurrentIndex++;
                updateImage();
                
                usrAnswersList.add("square");
                
                if (mCurrentIndex == 9 ){
                	scoreNeitz();
                }
            }
        });	
        
        // This imageButton is for the diamond
        imgButton4 = (ImageButton)findViewById(R.id.imageButton4);
        imgButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	mCurrentIndex++;
                updateImage();
                
                usrAnswersList.add("diamond");
                
                if (mCurrentIndex == 9 ){
                	scoreNeitz();
                }
            }
        });	
        
        // This imageButton is for nothing
        imgButton5 = (ImageButton)findViewById(R.id.imageButton5);
        imgButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	mCurrentIndex++;
                updateImage();
                
                usrAnswersList.add("nothing");
                
                if (mCurrentIndex == 9 ){
                	scoreNeitz();
                }
            }
        });	
       
        
    }
  
    private void updateImage() {
        switch (mCurrentIndex){
        case 0:
        	display.setImageResource(R.drawable.n_1);
        	break;
        case 1:
        	display.setImageResource(R.drawable.n_2);
        	break;
        case 2:
        	display.setImageResource(R.drawable.n_3);
        	break;
        case 3:
        	display.setImageResource(R.drawable.n_4);
        	break;
        case 4:
        	display.setImageResource(R.drawable.n_5);
        	break;
        case 5:
        	display.setImageResource(R.drawable.n_6);
        	break;
        case 6:
        	display.setImageResource(R.drawable.n_7);
        	break;
        case 7:
        	display.setImageResource(R.drawable.n_8);
        	break;
        case 8:
        	display.setImageResource(R.drawable.n_9);
        	break;
        
        }
    }
    
    private void scoreNeitz(){
    	
    	if (realAnswersList.equals(usrAnswersList))
    	{
			//correctAnswer++;
			//scoreIshihara = "Passed";
        	//Toast.makeText(IshiharaTest.this, "You " + scoreAmsler + " the Test", Toast.LENGTH_SHORT).show();
			
			Intent scoreNeitz = new Intent(getApplicationContext(), NeitzScore.class);
			scoreNeitz.putExtra("PassFail","Passed");
        	//scoreIshihara.putExtra("corrects", correctAnswer);
        	startActivity(scoreNeitz);
			
    	}
		else
		{
			Intent scoreNeitz = new Intent(getApplicationContext(), NeitzScore.class);
			scoreNeitz.putExtra("PassFail","Failed");
        	//scoreIshihara.putExtra("corrects", correctAnswer);
        	startActivity(scoreNeitz);
		}
    	
    	
    }
}
