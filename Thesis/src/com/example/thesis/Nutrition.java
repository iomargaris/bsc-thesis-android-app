package com.example.thesis;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Nutrition extends Activity {
	
	ListView list;
	ImageView image_nutr ;
	ImageButton mCarrot;
	ImageButton mOrange;
	String [] TitlesNutr;
	String [] DescriptionsNutr;
	int[] imagesNutr={R.drawable.fish, R.drawable.egg2, R.drawable.broccoli, R.drawable.carrot, R.drawable.orange};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.nutrition_layout);
        
        //edw exw tin eikona mou
        image_nutr = (ImageView)findViewById(R.id.image_nutr);
        //image_nutr.setImageResource(R.drawable.amsler);
        
        Resources res = getResources();
        TitlesNutr =res.getStringArray(R.array.Categories);
        DescriptionsNutr =res.getStringArray(R.array.DescriptionsNutr);
        
        list=(ListView)findViewById(R.id.listNutrition);
        MyAdapter adapter = new MyAdapter(this, TitlesNutr, imagesNutr, DescriptionsNutr);
        list.setAdapter(adapter);
        
        list.setOnItemClickListener( new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "hello john", Toast.LENGTH_SHORT).show();
				switch(position){
				case 0:
					/*Intent ishiharaIntent = new Intent(view.getContext(), IshiharaTest.class);
					startActivityForResult(ishiharaIntent, 0);
					break;*/
				case 1:
					/*Intent ishiharaIntent = new Intent(view.getContext(), IshiharaTest.class);
					startActivityForResult(ishiharaIntent, 0);
					break;*/
				case 2:
					/*Intent ishiharaIntent = new Intent(view.getContext(), IshiharaTest.class);
					startActivityForResult(ishiharaIntent, 0);
					break;*/
					
				case 3:
					/*Intent ishiharaIntent = new Intent(view.getContext(), IshiharaTest.class);
					startActivityForResult(ishiharaIntent, 0);
					break;*/
				case 4:
					/*Intent ishiharaIntent = new Intent(view.getContext(), IshiharaTest.class);
					startActivityForResult(ishiharaIntent, 0);
					break;*/
				/*case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:*/
				}
			}
        });
        
        
        class MyAdapter extends ArrayAdapter<String>
        {
        	Context context;
        	int[] images;
        	String[] titleArray;
        	String[] descriptionArray;
        	MyAdapter(Context c, String[] Titles, int imgs[], String[] desc)
        	{
        		super(c, R.layout.single_row_nutrition, R.id.textView1, Titles);
        		this.context=c;
        		this.images=imgs;
        		this.titleArray=Titles;
        		this.descriptionArray=desc;
        	}
        	
        	@Override
        	public View getView(int position, View convertView, ViewGroup parent){
        		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        		View row=inflater.inflate(R.layout.single_row_nutrition, parent, false);
        		
        		ImageView myImage=(ImageView)row.findViewById(R.id.imageView1);
        		TextView myTitle=(TextView) row.findViewById(R.id.textView1);
        		TextView myDescription=(TextView) row.findViewById(R.id.textView2);
        		
        		myImage.setImageResource(images[position]);
        		myTitle.setText(titleArray[position]);
        		myDescription.setText(descriptionArray[position]);
        		
        		return row;
        	}
        }

    	}
}
