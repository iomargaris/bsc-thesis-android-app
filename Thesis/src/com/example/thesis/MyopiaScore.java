package com.example.thesis;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MyopiaScore extends Activity {
	
	TextView textV ;
	String result;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.myopia_score_layout);
        
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("PassFail");
        
        textV = (TextView)findViewById(R.id.myopiaScore);

        
        
        
        if(value.equals("red") ){
        	//result = "Congratulations, there are no signs to suggest macular damage";
        	//Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        	textV.setText(R.string.myopiaRed);
        }
        else if(value.equals("same") )
        {
        	//Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        	//result = "Sorry, the test revealed that you may suffer from macular damage. It is highly advisable to contact your ophthalmologist. ";
        	textV.setText(R.string.myopiaSame);
        }
        else if(value.equals("green"))
        {
        	textV.setText(R.string.myopiaGreen);
        }
        
        
        
    	}
}
