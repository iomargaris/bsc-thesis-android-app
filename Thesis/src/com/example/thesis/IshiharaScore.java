package com.example.thesis;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class IshiharaScore extends Activity {
		
	TextView textV ;
	String result;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.ishihara_score_layout);
        
        //Get extras from the intent that was called
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("PassFail");
        //int score = extras.getInt("corrects");
        
        textV = (TextView)findViewById(R.id.ishiharaScore);

     
        if(value.equals("Passed") ){
        	//result = "Congratulations, there are no signs to suggest macular damage";
        	//Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        	textV.setText(R.string.ishiharaPassed);
        }
        else if(value.equals("Failed") )
        {
        	//Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        	//result = "Sorry, the test revealed that you may suffer from macular damage. It is highly advisable to contact your ophthalmologist. ";
        	textV.setText(R.string.ishiharaFailed);
        }
        
        
        
    }
}
