package com.example.thesis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;


public class Myopia extends Activity  {
	
	ImageView display ;
	ImageView display2 ;
	Button mRedButton;
	Button mGreenButton;
	Button mSameButton;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.myopia_layout);
        
        display = (ImageView)findViewById(R.id.display_myopia);
        display.setImageResource(R.drawable.myopia3);
        
        display2 = (ImageView)findViewById(R.id.display_myopia2);
        display2.setImageResource(R.drawable.myopia2);
        
        mRedButton = (Button)findViewById(R.id.red_button);
        mRedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent scoreMyopia = new Intent(getApplicationContext(), MyopiaScore.class);
            	scoreMyopia.putExtra("PassFail","red");
            	startActivity(scoreMyopia);
            }
        });	
        mSameButton = (Button)findViewById(R.id.same_button);
        mSameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent scoreMyopia = new Intent(getApplicationContext(), MyopiaScore.class);
            	scoreMyopia.putExtra("PassFail","same");
            	startActivity(scoreMyopia);
            }
        });	
        
        mGreenButton = (Button)findViewById(R.id.green_button);
        mGreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent scoreMyopia = new Intent(getApplicationContext(), MyopiaScore.class);
            	scoreMyopia.putExtra("PassFail","green");
            	startActivity(scoreMyopia);
            }
        });	
        
        
        
        
        
    }
  
}
