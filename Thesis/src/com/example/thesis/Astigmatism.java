package com.example.thesis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


public class Astigmatism extends Activity  {
	
	ImageView display ;
	EditText input;
	Button mYesButton;
	Button mNoButton;
	int mCurrentIndex = 0;
	int yesCounter=0;
	int noCounter=0;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.astigmatism_layout);
        
        display = (ImageView)findViewById(R.id.display_astigmatism);
        updateImage();
        
        EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        
        mYesButton = (Button)findViewById(R.id.yes_button);
        mYesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	yesCounter++;
            	mCurrentIndex++;
                //mCurrentIndex = (mCurrentIndex + 1) % 5;
       
                updateImage();
                showResult();
            }
        });	
        
        mNoButton = (Button)findViewById(R.id.no_button);
        mNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	noCounter++;
            	mCurrentIndex++;
                //mCurrentIndex = (mCurrentIndex + 1) % 5;
                
                updateImage();
                showResult();
            }
            
        });	
        
    }
    

	private void updateImage() {
        switch (mCurrentIndex){
        case 0:
        	display.setImageResource(R.drawable.ast1);
        	break;
        case 1:
        	display.setImageResource(R.drawable.ast2);
        	break;
        case 2:
        	display.setImageResource(R.drawable.ast3);
        	break;
        case 3:
        	display.setImageResource(R.drawable.ast4);
        	break;
        case 4:
        	display.setImageResource(R.drawable.ast5);
        	break;
        }
        
    }
	
	private void showResult() {
		
		if(yesCounter == 5 && noCounter == 0){
        	Intent scoreAstigmatism = new Intent(getApplicationContext(), AstigmatismScore.class);
        	scoreAstigmatism.putExtra("PassFail","Passed");
        	startActivity(scoreAstigmatism);
        }
		
		if( (noCounter+yesCounter) == 5 && noCounter != 0 ){
         	Intent scoreAstigmatism = new Intent(getApplicationContext(), AstigmatismScore.class);
         	scoreAstigmatism.putExtra("PassFail","Failed");
         	startActivity(scoreAstigmatism);
         }
		
	}
  
}
