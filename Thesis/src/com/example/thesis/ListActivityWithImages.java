package com.example.thesis;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ListActivityWithImages extends Activity {
	
	ListView list;
	String [] Titles;
	String [] Descriptions;
	int[] images={R.drawable.ishihara_list_icon, R.drawable.neitz_list_icon, R.drawable.amsler_list_icon, R.drawable.ast_list_icon, R.drawable.myopia_list_icon};
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        
        Resources res = getResources();
        Titles =res.getStringArray(R.array.Tests);
        Descriptions =res.getStringArray(R.array.Descriptions);
        
        list=(ListView)findViewById(R.id.listView1);
        
    	// Add header to the list
 		View header = getLayoutInflater().inflate(R.layout.header_listview, null);
 		list.addHeaderView(header);
        
        MyAdapter adapter = new MyAdapter(this, Titles, images, Descriptions);
        list.setAdapter(adapter);
        
        
        list.setOnItemClickListener( new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "hello john", Toast.LENGTH_SHORT).show();
				switch(position){
				case 1:
					Intent ishiharaIntent = new Intent(view.getContext(), IshiharaTest.class);
					startActivityForResult(ishiharaIntent, 0);
					break;
				case 2:
					Intent neitzIntent = new Intent(view.getContext(), NeitzTest.class);
					startActivityForResult(neitzIntent, 0);
					break;
				case 3:
					Intent amslerIntent = new Intent(view.getContext(), Amsler.class);
					startActivityForResult(amslerIntent, 0);
					break;
					
				case 4:
					Intent astigmatismIntent = new Intent(view.getContext(), Astigmatism.class);
					startActivityForResult(astigmatismIntent, 0);
					break;
				case 5:
					Intent myopiaIntent = new Intent(view.getContext(), Myopia.class);
					startActivityForResult(myopiaIntent, 0);
					break;
						/*case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:*/
				}
			}
        });
        
		}
}

class MyAdapter extends ArrayAdapter<String>
{
	Context context;
	int[] images;
	String[] titleArray;
	String[] descriptionArray;
	MyAdapter(Context c, String[] Titles, int imgs[], String[] desc)
	{
		super(c, R.layout.single_row, R.id.textView1, Titles);
		this.context=c;
		this.images=imgs;
		this.titleArray=Titles;
		this.descriptionArray=desc;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row=inflater.inflate(R.layout.single_row, parent, false);
		
		ImageView myImage=(ImageView)row.findViewById(R.id.imageView1);
		TextView myTitle=(TextView) row.findViewById(R.id.textView1);
		TextView myDescription=(TextView) row.findViewById(R.id.textView2);
		
		myImage.setImageResource(images[position]);
		myTitle.setText(titleArray[position]);
		myDescription.setText(descriptionArray[position]);
		
		return row;
	}
}









