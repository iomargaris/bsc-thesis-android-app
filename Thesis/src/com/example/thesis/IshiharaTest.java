package com.example.thesis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


public class IshiharaTest extends Activity  {
	
	//Declaring variables
	ImageView display ;
	EditText input;
	Button mNextButton;
	int mCurrentIndex = 0;
	
	int realAnswers[] = {12,15,16,26,29,3,42,45,5,6,7,73,74,8};
	List<Integer> realAnswersList = new ArrayList<Integer>();
	int usrAnswers[];
	List<Integer> usrAnswersList = new ArrayList<Integer>();
	
	String scoreIshihara;
	int correctAnswer;
	int number;
	int counter=0;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //make activity full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Populate the layout
        setContentView(R.layout.ishihara_layout);
        
        display = (ImageView)findViewById(R.id.display);
        updateImage();
        
        //Read what the user inserted
    	input = (EditText)findViewById(R.id.input);
        //input.setInputType(InputType.TYPE_CLASS_NUMBER);
        
    	//Populate the list with the correct answers
        for (int index = 0; index < realAnswers.length; index++)
        {
            realAnswersList.add(realAnswers[index]);
        }
        
        mNextButton = (Button)findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
            	
            	number = Integer.parseInt(input.getText().toString());
            	usrAnswersList.add(number);
            	
            	//usrAnswers[counter] = number;
            	//counter++;
            	
            	Toast.makeText(IshiharaTest.this, "the number you selected was :"+ number, Toast.LENGTH_SHORT).show();
            	
                mCurrentIndex++;
                updateImage();
                
                if (mCurrentIndex == 14 ){
                	
                	scoreIshihara();
                	
                }
                
            }
        });	
       
    }

    private void scoreIshihara() {
    		
		if (realAnswersList.equals(usrAnswersList))
    	{
			//correctAnswer++;
			//scoreIshihara = "Passed";
        	//Toast.makeText(IshiharaTest.this, "You " + scoreAmsler + " the Test", Toast.LENGTH_SHORT).show();
			
			Intent scoreIshihara = new Intent(getApplicationContext(), IshiharaScore.class);
        	scoreIshihara.putExtra("PassFail","Passed");
        	//scoreIshihara.putExtra("corrects", correctAnswer);
        	startActivity(scoreIshihara);
			
    	}
		else
		{
			Intent scoreIshihara = new Intent(getApplicationContext(), IshiharaScore.class);
        	scoreIshihara.putExtra("PassFail","Failed");
        	//scoreIshihara.putExtra("corrects", correctAnswer);
        	startActivity(scoreIshihara);
		}
    }
    
    private void updateImage() {
        switch (mCurrentIndex){
        case 0:
        	display.setImageResource(R.drawable.is_12);
        	break;
        case 1:
        	display.setImageResource(R.drawable.is_15);
        	break;
        case 2:
        	display.setImageResource(R.drawable.is_16);
        	break;
        case 3:
        	display.setImageResource(R.drawable.is_26);
        	break;
        case 4:
        	display.setImageResource(R.drawable.is_29);
        	break;
        case 5:
        	display.setImageResource(R.drawable.is_3);
        	break;
        case 6:
        	display.setImageResource(R.drawable.is_42);
        	break;
        case 7:
        	display.setImageResource(R.drawable.is_45);
        	break;
        case 8:
        	display.setImageResource(R.drawable.is_5);
        	break;
        case 9:
        	display.setImageResource(R.drawable.is_6);
        	break;
        case 10:
        	display.setImageResource(R.drawable.is_7);
        	break;
        case 11:
        	display.setImageResource(R.drawable.is_73);
        	break;
        case 12:
        	display.setImageResource(R.drawable.is_74);
        	break;
        case 13:
        	display.setImageResource(R.drawable.is_8);
        	break;
        }
    }
    
    
}
