# Welcome

This is a repository of my BSc Thesis files. I graduated from Technical University of Crete, Electronic and Computer Engineering in 2016.

An investigational native android application for eye tests. The purpose of this app was to provide an early indication to users on their eye-site condition.  

**Disclaimer**: This is not meant to provide any diagnosis or to replace a visit to your doctor!

In this repo you can find all the source files, the thesis pdf that was submitted (Grade 10/10) and the .apk file if you wish to install on your smartphone and test this app out!

**Note**: To install the .apk on your smartphone, you will need to first enable the "Unknown sources" setting from the settings menu. (The name of this setting may vary for different versions of android)

Thanks!